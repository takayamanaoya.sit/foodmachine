import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane orderItemList;
    private JButton sushiButton;
    private JButton gyudonButton;
    private JButton sobaButton;
    private JButton cancelButton;
    private JLabel totalCost;
    private JTabbedPane tabbedPane1;
    private JButton takeOutTempuraButton;
    private JButton takeOutRamenButton;
    private JButton takeOutUdonButton;
    private JButton takeOutSushiButton;
    private JButton takeOutGyudonButton;
    private JButton takeOutSobaButton;
    private JLabel takeOutTotalCost;
    private JTextPane takeOutOrderedItemList;
    private JButton takeOutCancelButton;
    private JButton checkOutButton;
    private JButton takeOutCheckOutButton;

    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura","900");
            }
        });
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("tempura.jpg")));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen","800");
            }
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon","400");
            }
        });
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("udon.jpg")));

        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi","1000");
            }
        });
        sushiButton.setIcon(new ImageIcon(this.getClass().getResource("sushi.jpg")));

        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba","550");
            }
        });
        sobaButton.setIcon(new ImageIcon(this.getClass().getResource("soba.jpg")));

        gyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyudon","380");
            }
        });
        gyudonButton.setIcon(new ImageIcon(this.getClass().getResource("gyudon.jpg")));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    int couponConfirmation = JOptionPane.showConfirmDialog(null,
                            "Do yo have 200yen discount coupon?",
                            "Coupon Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if (couponConfirmation == 0){
                        String currentCost= totalCost.getText();
                        int useCouponCost = Integer.parseInt(currentCost, 10) -200;
                        totalCost.setText(String.valueOf(useCouponCost));
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is "+ String.valueOf(useCouponCost) + " yen.\n" +
                                        "When you pass the ticket, please show the coupon to the clerk.");

                        orderItemList.setText("");
                        totalCost.setText("0");
                    }
                    else if (couponConfirmation == 1){

                        String currentCost= totalCost.getText();
                        JOptionPane.showMessageDialog(null,
                                "Thank you.The total price is "+ currentCost + " yen.");

                        orderItemList.setText("");
                        totalCost.setText("0");
                    }
                }
                else if (confirmation == 1) {
                    JOptionPane.showMessageDialog(null,
                            "If you want to eat in, please switch tabs at upper left.");
                }
            }
        });


        takeOutTempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                takeoutorder("Tempura","900");
            }
        });
        takeOutTempuraButton.setIcon(new ImageIcon(this.getClass().getResource("tempura.jpg")));

        takeOutRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                takeoutorder("Ramen","800");
            }
        });
        takeOutRamenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));

        takeOutUdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                takeoutorder("Udon","400");
            }
        });
        takeOutUdonButton.setIcon(new ImageIcon(this.getClass().getResource("udon.jpg")));

        takeOutSobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                takeoutorder("Soba","550");
            }
        });
        takeOutSobaButton.setIcon(new ImageIcon(this.getClass().getResource("soba.jpg")));

        takeOutGyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                takeoutorder("Gyudon","380");
            }
        });
        takeOutGyudonButton.setIcon(new ImageIcon(this.getClass().getResource("gyudon.jpg")));

        takeOutSushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                takeoutorder("Sushi","1000");
            }
        });
        takeOutSushiButton.setIcon(new ImageIcon(this.getClass().getResource("sushi.jpg")));

        takeOutCheckOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    int couponConfirmation = JOptionPane.showConfirmDialog(null,
                            "Do yo have 200yen discount coupon?",
                            "Coupon Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if (couponConfirmation == 0){
                        String takeOutCurrentCost= takeOutTotalCost.getText();
                        int useCouponCost = Integer.parseInt(takeOutCurrentCost, 10) -200;
                        takeOutTotalCost.setText(String.valueOf(useCouponCost));
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is "+ String.valueOf(useCouponCost) + " yen.\n" +
                                        "When you pass the ticket, please show the coupon to the clerk.");

                        takeOutOrderedItemList.setText("");
                        takeOutTotalCost.setText("0");
                    }
                    else if (couponConfirmation == 1){

                        String takeOutCurrentCost= takeOutTotalCost.getText();
                        JOptionPane.showMessageDialog(null,
                                "Thank you.The total price is "+ takeOutCurrentCost + " yen.");

                        takeOutOrderedItemList.setText("");
                        takeOutTotalCost.setText("0");
                    }
                    }

                else if (confirmation == 1) {
                    JOptionPane.showMessageDialog(null,
                            "If you want to eat in, please switch tabs at upper left.");
                }

            }
        });
        takeOutCancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel your order?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    takeOutOrderedItemList.setText("");
                    takeOutTotalCost.setText("0");
                }

            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel your order?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    orderItemList.setText("");
                    totalCost.setText("0");
                }
            }
        });
    }

    void order(String food,String foodCost) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order confrmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            String currentText= orderItemList.getText();
            orderItemList.setText(currentText + food + " " + foodCost + "yen.\n");

            String currentCost= totalCost.getText();
            int costSum=Integer.parseInt(currentCost,10)+Integer.parseInt(foodCost,10);
            totalCost.setText(String.valueOf(costSum));

            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");

        }
    }

    void takeoutorder(String food,String foodCost) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order confrmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            String takeOutCurrentText= takeOutOrderedItemList.getText();
            takeOutOrderedItemList.setText(takeOutCurrentText + food + " " + foodCost + "yen.\n");

            String takeOutCurrentCost= takeOutTotalCost.getText();
            int costSum=Integer.parseInt(takeOutCurrentCost,10)+Integer.parseInt(foodCost,10);
            takeOutTotalCost.setText(String.valueOf(costSum));

            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");

        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}